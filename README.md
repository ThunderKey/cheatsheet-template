# Cheatsheet Template

Source: https://www.overleaf.com/latex/templates/colourful-cheatsheet-template/qdsshbjktndd

## Getting started

Put this Repo in the folder `cheatsheet-template`.

```tex
\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}

\usepackage[landscape,margin=1cm]{geometry}
\usepackage[english]{babel}

\input{cheatsheet-template/template.tex}

\begin{document}
  \begin{TinyText}
    \begin{multicols}{4}
      \section{YOUR SECTION TITLE}
      \begin{textbox}{YOUR TEXT BOX TITLE}
        YOUR TEXT BOX CONTENT
      \end{textbox}
    \end{multicols}
  \end{TinyText}
\end{document}
```
